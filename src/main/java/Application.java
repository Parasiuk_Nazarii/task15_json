import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import domain.Gem;
import service.parserJSON.GoogleJSONParser;
import service.parserJSON.JacksonParser;
import service.validator.JSONGemsValidator;

import java.io.IOException;
import java.util.List;

public class Application {
    public static void main(String[] args) throws IOException, ProcessingException {
        System.out.println(new JSONGemsValidator().isValidJsonGems());
        List<Gem> gems = new GoogleJSONParser().getGemsList();
        new GoogleJSONParser().writeToFile();
        new JacksonParser().writeToFile();
        gems.forEach(System.out::println);
    }
}
