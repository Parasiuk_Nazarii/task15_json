package service.parserJSON;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import domain.Gem;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class GoogleJSONParser {
    private Gson json;

    public GoogleJSONParser() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        json = gsonBuilder.create();
    }

    public List<Gem> getGemsList() {
        Gem[] gems = new Gem[0];
        try {
            gems = json.fromJson(new FileReader(new File("src/main/resources/json/gems.json")), Gem[].class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Arrays.asList(gems);
    }

    public void writeToFile() {
        try {
            Writer writer = new FileWriter("src/main/resources/out/JSONGoogleOut.txt");
            getGemsList().forEach(g -> {
                json.toJson(g, writer);
                try {
                    writer.append(System.getProperty("line.separator"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
