package domain;


public class VisualParameters {
    private String color;
    private double transparency;
    private int faces;


    public VisualParameters() {
    }

    public VisualParameters(String color, double transparency, int faces) {
        this.color = color;
        this.transparency = transparency;
        this.faces = faces;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getTransparency() {
        return transparency;
    }

    public void setTransparency(double transparency) {
        this.transparency = transparency;
    }

    public int getNumberOfFaces() {
        return faces;
    }

    public void setNumberOfFaces(int numberOfFaces) {
        this.faces = numberOfFaces;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "color='" + color + '\'' +
                ", transparency=" + transparency +
                ", numberOfFaces=" + faces +
                '}';
    }
}
