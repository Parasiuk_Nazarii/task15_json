import domain.Gem;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Test;

public class TestJSON {
    @Test
    public void validate() throws ValidationException {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(Gem.class.getResourceAsStream("src/main/resources/json/gemsSchema.json")));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(Gem.class.getResourceAsStream("src/main/resources/json/gems.json")));

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }
}
